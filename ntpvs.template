record("*", "$(NDARRAY_WAVE)ArrayData")
{
    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            +id:"epics:nt/NTNDArray:1.0",
            "value":{+type:"any",
                     +channel:"VAL"},
            "":{+type:"meta", +channel:"SEVR"}
        }
    })

    field(TSEL, "$(PEVR)ModStCnt-I.TIME CA")
    field(TSE, "0")
}

record("*", "$(NDARRAY_WAVE)ArraySize0_RBV") {
    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "dimension[0].size":{+channel:"VAL", +type:"plain", +putorder:0}
        }
    })
}

record("*", "$(NDARRAY_WAVE)UniqueId_RBV") {
    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "uniqueId":{+channel:"VAL", +type:"plain", +putorder:0}
        }
    })
}

record("*", "$(NDARRAY_WAVE)TimeStamp_RBV") {
    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "dataTimeStamp":{+channel:"VAL", +type:"plain", +putorder:0}
        }
    })
}

#- #######################################
#- Triggering Events params
#- #######################################
record(calc, "$(P)$(R)#TrgEvtIn") {
    field(DESC, "Trig_Evt_Input")
    field(VAL,  "0")

    #- Input used only to force processing of this record
    field(INPA, "$(PEVR)ModStCnt-I CPP")

    #- Event Number of the trigger line
    field(INPB, "$(PEVR)DlyGen-6-Evt-Trig0-SP CA")
    field(CALC, "B+0")
    
    #- timestamp of this record should be the timestamp of the trigger    
    field(TSEL, "$(PEVR)ModStCnt-I.TIME CA")

    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "attribute[0].value":{+type:"any", +channel:"VAL"},
            "attribute[0].name":{+type:"plain", +channel:"DESC"},
            "attribute[0].timeStamp":{+type:"meta", +channel:"TIME"},
        }
    })
}

#- #######################################
#- Pulse ID params
#- #######################################
record(int64in, "$(P)$(R)#PulseIdIn") {
    field(DESC, "IDCycle")
    field(INP, {pva:{pv:"$(PEVR)IdCycle",proc:true,time:true}})
    field(TSE, "-2")
    
    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "attribute[1].name":{+type:"plain", +channel:"DESC"},
            "attribute[1].value":{+type:"any", +channel:"VAL"},
            "attribute[1].timeStamp":{+type:"meta", +channel:"TIME"},
        }
    })
}

#- #######################################
#- SDS trigger and EVR timestamp
#- #######################################
record(calc, "$(P)$(R)#SDSTrig") {
    field(DESC, "SDSCounter")
    field(CALC, "B+1")
    field(VAL,  "0")
    field(INPA, "$(PEVR)SDSCnt-I CPP")
    field(INPB, "$(P)$(R)#SDSTrig NPP")
    field(TSEL, "$(PEVR)SDSCnt-I.TIME CA")

    info(Q:group, {
        "$(P)$(R)NTNDA-SDS":{
            "attribute[2].value":{+type:"any", +channel:"VAL",+trigger:"*"},
            "attribute[2].name":{+type:"plain", +channel:"DESC"},
            "attribute[2].timeStamp":{+type:"meta", +channel:"TIME"},
        }
    })
}


