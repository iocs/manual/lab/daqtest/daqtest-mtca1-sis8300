#- common E3 modules
require busy
require calc
require essioc

#- Struck sis8300 modules
require asyn
require adcore
require admisc
require adsis8300

#- ----------------------------------------------------------------------------
#- General Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("SYSSUB",       "LabS-ICS")
epicsEnvSet("EVR_DEV",      "Ctrl-EVR-411")
epicsEnvSet("STRUCK_DEV",   "Ctrl-AMC-411")
epicsEnvSet("IOCDEV",       "SC-IOC-412")

#- ----------------------------------------------------------------------------
#- essioc Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("IOCNAME",       "$(SYSSUB):$(IOCDEV)")
epicsEnvSet("IOCDIR",        "$(SYSSUB)_$(IOCDEV)")
#epicsEnvSet("AS_TOP",        "/tmp")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- ----------------------------------------------------------------------------
#- Struck SIS8300 Configuration
#- ----------------------------------------------------------------------------
epicsEnvSet("CONTROL_GROUP", "$(SYSSUB)")
epicsEnvSet("AMC_NAME",      "$(STRUCK_DEV)")
epicsEnvSet("AMC_DEVICE",    "/dev/sis8300-6")
epicsEnvSet("EVR_NAME",      "$(EVR_DEV):")
epicsEnvSet("PREFIX",        "$(SYSSUB):$(AMC_NAME):")

#- Additions to create SDS PVs
epicsEnvSet("ents_DB", "$(E3_CMD_TOP)/../e3-ents/entsApp/Db")
epicsEnvSet("PEVR", "LabS-ICS:Ctrl-EVR-411:")
epicsEnvSet("PEVM", "LabS-ICS:Ctrl-EVM-441:")

#- Load channels
iocshLoad("$(E3_CMD_TOP)/iocsh/sis8300.iocsh")

#- ----------------------------------------------------------------------------
#- IOC Start
#- ----------------------------------------------------------------------------
iocInit()

